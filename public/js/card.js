jQuery(document).ready(function ($) {
    jQuery('select').each(function () {
        jQuery(this).on('change', function () {
            var selVal = jQuery(this).val();
            var selName = jQuery(this).attr('name');
            jQuery("div[id=" + selName + "]").text(selVal);
        }).change();
    });

    var $image = jQuery('#photo');

    $image.on('change', function (e) {
        if (window.FileReader) {

            var file = e.target.files[0];
            var fileReader = new FileReader();

            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function() {
                    $image.next( 'label').html( 'Загрузить другое фото' );
                    jQuery('#main-image').html('<img src="' + this.result + '" />'); 
                }
            }
        }
    });
});

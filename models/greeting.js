const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GreetingSchema = new Schema({
        welcome: {type: String, default: '', trim: true},
        who: {type: String, default: '', trim: true},
        which: {type: String, default: '', trim: true},
        image: {type: String, default: '', trim: true},
        date: {type: Date, default: Date.now}
    },
    {versionKey: false});

module.exports = mongoose.model('Greeting', GreetingSchema);
var express = require('express');
var router = express.Router();
// file upload handling
var multer = require('multer');
var upload = multer({dest: '../public/uploads/'});

// mongodb model
var Greeting = require('../models/greeting.js');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {error: null});
});

router.get('/card/:id', function (req, res, next) {
    Greeting.findById(req.params.id, function (error, data) {
        if (error)
            res.send(err);

        if (!data)
            res.sendStatus(404);
        else
            res.render('result',{object: data});
    });
});


router.post('/', upload.single('photo'), function (req, res, next) {
    // handle if user didnt select image
/*    if (typeof(req.file) === 'undefined') {
        return res.render('index', {error: 'Выберите изображение перед сохранением.'});
    }*/

    var welcome = req.body.wellcome;
    var who = req.body.who;
    var which = req.body.wish;
    var image = "";

    if (typeof(req.file) !== 'undefined') {
        image = req.file.filename;
    }



    var greeting = new Greeting({welcome: welcome, who: who, which: which, image: image});
    greeting.save(function(err, item) {
        if (err)
            res.send(err);

        res.redirect('/card/' + item._id);
    });


});

module.exports = router;
